# json-to-jsdoc-converter-webui

## [Click here  to open app demo](https://json-to-jsdoc-converter-webui.000webhostapp.com/)

## This is small project that provides UI representation of "json to jsDocs converter" package: [link to the package](https://www.npmjs.com/package/json-to-jsdoc-converter)

## CLI Commands

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run start

# build for production with minification
npm run build

# test the production build locally
npm run serve

# run tests with jest and enzyme
npm run test
```

For detailed explanation on how things work, checkout
the [CLI Readme](https://github.com/developit/preact-cli/blob/master/README.md).
