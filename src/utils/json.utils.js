/**
 * @param {String} json
 * @returns {boolean}
 */
export const isValidJson = (json) => {
  try {
    JSON.parse(json);
    return true;
  }
  catch (err) {
    return false;
  }
};