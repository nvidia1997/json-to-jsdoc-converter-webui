import { useRef, useState } from "preact/hooks";
import { isValidJson } from "../../utils/json.utils";
import { JsonToJsdocConverter } from "json-to-jsdoc-converter";
import clsx from "clsx";
import style from './style.css';

const Home = () => {
  const [typePrefix, setTypePrefix] = useState("");
  const [typeSuffix, setTypeSuffix] = useState("");
  const [jsonValue, setJsonValue] = useState("");
  const [jsDocsValue, setJsDocsValue] = useState("");
  
  const isJsonValid = isValidJson(jsonValue);
  const jsonToJsdocConverterRef = useRef(new JsonToJsdocConverter());
  
  const _onClick = () => {
    if (isJsonValid) {
      setJsDocsValue(
        jsonToJsdocConverterRef.current.convert(
          jsonValue,
          {
            typesPrefix: typePrefix.trim(),
            typesSuffix: typeSuffix.trim(),
          },
        ),
      );
    }
    else {
      alert("Invalid JSON");
    }
  };
  
  return (
    <div class={style.container}>
      <div class={style.configInputsContainer}>
        <div class={style.typePrefix}>
          <label for="type-prefix">types prefix:</label>
          <input
            name="type-prefix"
            type="text"
            oninput={({ target: { value } }) => setTypePrefix(value)}
          />
        </div>
        
        <div class={style.typeSuffix}>
          <label for="type-suffix">types suffix:</label>
          <input
            name="type-suffix"
            type="text"
            oninput={({ target: { value } }) => setTypeSuffix(value)}
          />
        </div>
      </div>
      
      <div class={style.textAreasContainer}>
         <textarea
           placeholder="JSON"
           oninput={({ target: { value } }) => setJsonValue(value)}
           value={jsonValue}
         />
        
        <textarea
          placeholder="JS Doc"
          value={jsDocsValue}
        />
      </div>
      
      {!isJsonValid && <p class={style.errorText}>Invalid JSON</p>}
      
      <button
        class={clsx({
          [style.convertBtn]: true,
          [style.convertBtnDisabled]: isJsonValid === false,
        })}
        onclick={_onClick}
        disabled={isJsonValid ? undefined : "disabled"}
      >
        Convert
      </button>
    </div>
  );
};

export default Home;
